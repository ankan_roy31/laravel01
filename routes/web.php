<?php

use App\Http\Controllers\Admin\MeetingController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\StudentController as ControllersStudentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home/{name}', [MeetingController::class, 'edit']);
Route::get('/editadmin', [StudentController::class, 'home']);
Route::get('/edit', [ControllersStudentController::class, 'home']);
Route::get('/frist', [ControllersStudentController::class, 'frist']);
Route::get('/second', [ControllersStudentController::class, 'second']);
